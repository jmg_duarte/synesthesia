package synth;

import ddf.minim.AudioOutput;
import ddf.minim.Minim;
import ddf.minim.ugens.Frequency;
import ddf.minim.ugens.Oscil;
import ddf.minim.ugens.Waves;
import processing.core.PApplet;

public class Synth {

	private static final float ANGLE_STARTING_VALUE = 0f;
	private static final float ANGLE_STEP_VALUE = 0.5f;
	private static final float SYNTH_RADIUS_VALUE = 150;
	
	private PApplet p;
	private Minim minim;
	private AudioOutput output;
	private Oscil wave;
	private float angleStep;
	
	//---Synth Variables---
	private float synthRadius;
	private float synthXCoord;
	private float synthYCoord;

	public Synth(PApplet p, Frequency freq, float ampl) {
		this.p = p;
		
		minim = new Minim(this);
		
		output = minim.getLineOut();
		
		wave = new Oscil(freq, ampl, Waves.SINE);
		wave.patch(output);
		
		//---Initialize Variables Bellow---
		this.angleStep = ANGLE_STARTING_VALUE;
		this.synthRadius = SYNTH_RADIUS_VALUE;
		this.synthXCoord = p.width / 2;
		this.synthYCoord = p.height / 2;
	}
	
	public Synth(PApplet p, Frequency freq, float ampl, float synthRadius) {
		this.p = p;
		
		minim = new Minim(this);
		
		output = minim.getLineOut();
		
		wave = new Oscil(freq, ampl, Waves.SINE);
		wave.patch(output);
		
		//---Initialize Variables Bellow---
		this.angleStep = ANGLE_STARTING_VALUE;
		this.synthRadius = synthRadius;
		this.synthXCoord = p.width / 2;
		this.synthYCoord = p.height / 2;
	}
	
	public Synth(PApplet p, Frequency freq, float ampl, float synthRadius, float xCoord, float yCoord) {
		this.p = p;
		
		minim = new Minim(this);
		
		output = minim.getLineOut();
		
		wave = new Oscil(freq, ampl, Waves.SINE);
		wave.patch(output);
		
		//---Initialize Variables Bellow---
		this.angleStep = ANGLE_STARTING_VALUE;
		this.synthRadius = synthRadius;
		this.synthXCoord = xCoord;
		this.synthYCoord = yCoord;
	}

	public void display() {
		//p.fill(255, 70);
		//p.rect(0, 0, p.width, p.height);

		for (int i = 0; i < output.bufferSize() - 1; i++) {
			p.fill(p.noise(p.random(255)) * 255, 255, 200);
			p.ellipse(xCoordinate(synthXCoord, synthRadius, angleStep, output.mix.get(i) + 1), yCoordinate(synthYCoord, synthRadius, angleStep, output.mix.get(i) + 1), 5,
					5);
			angleStep += ANGLE_STEP_VALUE;
		}
	}

	public void setSynthFrequency(Frequency newFreq){
		wave.setFrequency(newFreq);
	}
	
	public void setSynthAmplitude(float newAmp){
		wave.setAmplitude(newAmp);
	}
	
	private float xCoordinate(float xCoord, float radius, float angle, float amplitude) {
		return xCoord + (radius * amplitude * PApplet.cos(angle));
	}

	private float yCoordinate(float yCoord, float radius, float angle, float amplitude) {
		return yCoord + (radius * amplitude * PApplet.sin(angle));
	}

}
