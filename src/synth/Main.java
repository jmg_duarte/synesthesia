package synth;

import ddf.minim.AudioOutput;
import ddf.minim.Minim;
import ddf.minim.ugens.Frequency;
import ddf.minim.ugens.Oscil;
import ddf.minim.ugens.Waves;
import processing.core.PApplet;

public class Main extends PApplet{
	
	private static final int WINDOW_WIDTH = 500;
	private static final int WINDOW_HEIGHT = 500;
	
	private static final float ANGLE_STARTING_VALUE = 0f;
	private static final float ANGLE_STEP_VALUE = 0.5f;
	
	private Minim minim;
	private AudioOutput output;
	private Oscil wave;
	private float angle;
	
	private Synth synthO;
	private Synth[][] synths;
	
	public static void main(String[] args) {
		PApplet.main("synth.Main");
	}
	
	public void settings(){
		size(WINDOW_WIDTH, WINDOW_HEIGHT);
		smooth();
	}
	
	public void setup(){
		background(255);
		noStroke();
		
//		synthO = new Synth(this, Frequency.ofHertz(432), 0.5f, 100);
//		angle = ANGLE_STARTING_VALUE;
//		
//		minim = new Minim(this);
//		
//		output = minim.getLineOut();
//		
//		wave = new Oscil(Frequency.ofPitch("A4"), 0.5f, Waves.SINE);
//		wave.patch(output);
	}
	
	public void draw(){
//		
//		fill(255, 70);
//		rect(0, 0, width, height);
//		
//		for (int i = 0; i < output.bufferSize() - 1; i++) {
//			fill(noise(random(255)) * 255, 255, 200);
//			ellipse(xCoordinate(150, angle, output.mix.get(i) + 1),
//					yCoordinate(150, angle, output.mix.get(i) + 1), 5, 5);
//			angle += ANGLE_STEP_VALUE;
//		}
//		synthO.display();
	}
	
//	public void mouseMoved(){
//		float amp = map(mouseY, 0, height, 1, 0);
//		wave.setAmplitude(amp);
//		
//		float freq = map(mouseX, 0, width, 110, 880);
//		wave.setFrequency(freq);
//	}
//	
//	private float xCoordinate(float radius, float angle, float amplitude) {
//		return (width / 2) + (radius * amplitude * cos(angle));
//	}
//
//	private float yCoordinate(float radius, float angle, float amplitude) {
//		return (width / 2) + (radius * amplitude * sin(angle));
//	}
}
