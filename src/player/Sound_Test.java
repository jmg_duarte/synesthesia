//package player;
//
//import ddf.minim.*;
//import processing.core.PApplet;
//import processing.core.PFont;
//
//public class Sound_Test extends PApplet {
//
//	private static final int WINDOW_WIDTH = 500;
//	private static final int WINDOW_HEIGHT = 500;
//
//	private static final String FONT_TITLE = "Helvetica";
//	private static final int FONT_SIZE_TITLE = 20;
//	private static final String FONT_AUTHOR = "Helvetica";
//	private static final int FONT_SIZE_AUTHOR = 12;
//
//	private static final float ANGLE_STARTING_VALUE = 0f;
//	private static final float ANGLE_STEP_VALUE = 0.5f;
//
//	private static final int AUDIO_BUFFER_SIZE = 1024;
//
//	private static final int SKETCH_FRAME_RATE = 120;
//
//	private Minim minim;
//	private AudioPlayer audioFile;
//	private AudioMetaData audioMetaData;
//	private PFont fontTitle;
//	private PFont fontAuthor;
//
//	private float angle;
//
//	public static void main(String[] args) {
//		PApplet.main("player.Sound_Test");
//	}
//
//	public void settings() {
//		size(WINDOW_WIDTH, WINDOW_HEIGHT);
//		smooth();
//	}
//
//	public void setup() {
//		colorMode(HSB);
//		noStroke();
//		frameRate(SKETCH_FRAME_RATE);
//
//		angle = ANGLE_STARTING_VALUE;
//
//		background(255);
//
//		minim = new Minim(this);
//
//		audioFile = minim.loadFile("resources/Fleshgod Apocalypse - Healing Through War (Orchestral Version).mp3", AUDIO_BUFFER_SIZE);
//		audioFile.play();
//
//		audioMetaData = audioFile.getMetaData();
//
//		fontTitle = createFont(FONT_TITLE, FONT_SIZE_TITLE);
//		fontAuthor = createFont(FONT_AUTHOR, FONT_SIZE_AUTHOR);
//
//	}
//
//	public void draw() {
//		fill(255, 50);
//		rect(0, 0, width, height);
//
//		fill(30);
//		textFont(fontTitle);
//		text(audioMetaData.title(), 50, 50);
//
//		textFont(fontAuthor);
//		text(audioMetaData.author(), 50, 70);
//
//		for (int i = 0; i < audioFile.bufferSize() - 1; i++) {
//			// line(i, 125 + audioFile.left.get(i)*50, i+1, 125 +
//			// audioFile.left.get(i+1)*50);
//			// line(i, 375 + audioFile.right.get(i)*50, i+1, 375 +
//			// audioFile.right.get(i+1)*50);
//
//			fill(noise(random(255)) * 255, 255, 200);
//			ellipse(xCoordinate(150, angle, audioFile.mix.get(i) + 1),
//					yCoordinate(150, angle, audioFile.mix.get(i) + 1), 5, 5);
//			angle += ANGLE_STEP_VALUE;
//		}
//
//	}
//
//	private float xCoordinate(float radius, float angle, float amplitude) {
//		return (width / 2) + (radius * amplitude * cos(angle));
//	}
//
//	private float yCoordinate(float radius, float angle, float amplitude) {
//		return (width / 2) + (radius * amplitude * sin(angle));
//	}
//
//}
